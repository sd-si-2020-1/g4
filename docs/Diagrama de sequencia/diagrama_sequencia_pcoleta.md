# UC1:​ Consultar ponto de coleta 
@startuml
title **UC1 - CONSULTAR PONTO DE COLETA - FLUXO NORMAL**
autonumber
actor Usuário
boundary Frontend
participant Usuário as UsuárioREST <<rest module>>
database Database
Usuário -> Frontend : seleciona material desejado
Frontend -> UsuárioREST : GET /pontos {\n    "material": <material>\n}
UsuárioREST -> Database : busca pontos que coletam o material
Database -> UsuárioREST : resultado: <<object>><Ponto>
UsuárioREST -> Frontend : RESPONSE 200\n lista com Pontos
Frontend -> Usuário : exibe os pontos para o usuário
@enduml


@startuml
title **UC1 - CONSULTAR PONTO DE COLETA - PONTO DE COLETA NÃO ENCONTRADO**
autonumber
actor Usuário
boundary Frontend
participant Usuário as UsuárioREST <<rest module>>
database Database
Usuário -> Frontend : seleciona material desejado
Frontend -> UsuárioREST : GET /pontos {\n    "material": <material>\n}
UsuárioREST -> Database : busca pontos que coletam o material
Database -> UsuárioREST : resultado: <<object>><NULL>
UsuárioREST -> Frontend : RESPONSE 404 \nerror {NotFoundError}
Frontend -> Usuário : não existem pontos de coleta disponíveis \npara o material selecionado
@enduml


# UC2:​ Fornecer autenticação  
@startuml
title **UC2 - FORNECER AUTENTICAÇÃO - FLUXO NORMAL**
autonumber
actor Usuário
boundary Frontend
participant Usuário as UsuárioREST <<rest module>>
database Database
Usuário -> Frontend : fornece login e senha
Frontend -> UsuárioREST : POST /users {\n    "login": <login>,\n    "senha": <senha>\n}
UsuárioREST -> Database : procurar login e senha no banco de dados
Database -> UsuárioREST : resultado: <<object>><Usuário>
UsuárioREST -> UsuárioREST : verifica combinação login e senha\n<Usuário>.check_aut(<login>, <senha>)
UsuárioREST -> Frontend : inicia sessão
Frontend -> Usuário : RESPONSE 200\nredireciona para página\ninicial do sistema.
@enduml


@startuml
title **UC2 - FORNECER AUTENTICAÇÃO - ERRO DE LOGIN E SENHA**
autonumber
actor Usuário
boundary Frontend
participant Usuário as UsuárioREST <<rest module>>
database Database
Usuário -> Frontend : fornece login e senha
Frontend -> UsuárioREST : POST /users {\n    "login": <login>,\n    "senha": <senha>\n}
UsuárioREST -> Database : procurar login e senha no banco de dados
Database -> UsuárioREST : resultado: <<object>><Usuário>
UsuárioREST -> UsuárioREST : verifica combinação login e senha\n<Usuário>.check_aut(<login>, <senha>)
UsuárioREST -> Frontend : RESPONSE 401\n{ "error": <BadRequestError> } 
Frontend -> Usuário : login ou senha incorretos, tente novamente
@enduml


# UC3.1:​ Cadastrar ponto de coleta  
@startuml
title **UC3.1 - CADASTRAR PONTO DE COLETA - FLUXO NORMAL**
autonumber
actor Empresa
boundary Frontend
participant Empresa as EmpresaREST <<rest module>>
database Database
Empresa -> Frontend : cadastra ponto de coleta
Frontend -> EmpresaREST : POST /ponto {\n    "endereço": <endereço>,\n    "materiais": <materiais>,\n    "latitude": <latitude>,\n    "longitude": <longitude>,\n    "horario": <horario>\n}
EmpresaREST -> Database : insere <endereço>, <materiais>,\n<latitude>, <longitude>,\n<horario> no banco
Database -> EmpresaREST : resultado: <<object>><Ponto>
EmpresaREST -> Frontend : RESPONSE 200\n<<object>><Ponto>
Frontend -> Empresa : exibe o ponto cadastrado
@enduml


@startuml
title **UC3.1 - CADASTRAR PONTO DE COLETA - DADOS INCORRETOS**
autonumber
actor Empresa
boundary Frontend
participant Empresa as EmpresaREST <<rest module>>
database Database
Empresa -> Frontend : cadastra ponto de coleta
Frontend -> EmpresaREST : POST /ponto {\n    "endereço": <endereço>,\n    "materiais": <materiais>,\n    "latitude": <latitude>,\n    "longitude": <longitude>,\n    "horario": <horario>\n}
EmpresaREST -> Database : insere <endereço>, <materiais>,\n<latitude>, <longitude>,\n<horario> no banco
Database -> EmpresaREST : resultado: <false>
EmpresaREST -> Frontend : RESPONSE 400\n{ "error": <BadRequest> }
Frontend -> Empresa : alguns dados estão incorretos, \npor favor, preencha os campos novamente
@enduml


# UC3.2:​ Editar ponto de coleta  
@startuml
title **UC3.2 - EDITAR PONTO DE COLETA - FLUXO NORMAL**
autonumber
actor Empresa
boundary Frontend
participant Empresa as EmpresaREST <<rest module>>
database Database
Empresa -> Frontend : modifica ponto de coleta
Frontend -> EmpresaREST : PATCH /ponto {\n    "id": <id>,\n    "endereço": <endereço>,\n    "materiais": <materiais>,\n    "latitude": <latitude>,\n    "longitude": <longitude>,\n    "horario": <horario>\n}
EmpresaREST -> Database : atualiza <endereço>, <materiais>,\n<latitude>, <longitude>,\n<horario> no banco
Database -> EmpresaREST : resultado: <<object>><Ponto>
EmpresaREST -> Frontend : RESPONSE 200\n<<object>><Ponto>
Frontend -> Empresa : exibe o ponto modificado
@enduml


@startuml
title **UC3.2 - EDITAR PONTO DE COLETA - CONFLITO DE EDIÇÃO**
autonumber
actor Empresa
boundary Frontend
participant Empresa as EmpresaREST <<rest module>>
database Database
Empresa -> Frontend : modifica ponto de coleta
Frontend -> EmpresaREST : PATCH /ponto {\n    "id": <id>,\n    "endereço": <endereço>,\n    "materiais": <materiais>,\n    "latitude": <latitude>,\n    "longitude": <longitude>,\n    "horario": <horario>\n}
EmpresaREST -> Database : atualiza <endereço>, <materiais>,\n<latitude>, <longitude>,\n<horario> no banco
Database -> EmpresaREST : resultado: <false>
EmpresaREST -> Frontend : RESPONSE 400 \n{ "error": <<BadRequest>> }
Frontend -> Empresa : não foi possível editar o ponto de \ncoleta selecionado, tente novamente
@enduml


# UC3.3:​ Remover ponto de coleta
@startuml
title **UC3.3 - REMOVER PONTO DE COLETA - FLUXO NORMAL**
autonumber
actor Usuário
boundary Frontend
participant Usuário as UsuárioREST <<rest module>>
database Database
Usuário -> Frontend : seleciona o ponto a ser removido
Frontend -> UsuárioREST : DELETE /ponto {\n    "id": <id>\n}
UsuárioREST -> Database : alterar flag de ponto removido no banco
Database -> UsuárioREST : resultado: <true>
UsuárioREST -> Frontend : RESPONSE 200
Frontend -> Usuário : Exibe mensagem de sucesso
@enduml


@startuml
title **UC3.3 - REMOVER PONTO DE COLETA - PONTO DE COLETA NÃO ENCONTRADO**
autonumber
actor Usuário
boundary Frontend
participant Usuário as UsuárioREST <<rest module>>
database Database
Usuário -> Frontend : seleciona o ponto a ser removido
Frontend -> UsuárioREST : DELETE /ponto {\n    "id": <id>\n}
UsuárioREST -> Database : alterar flag de ponto removido no banco
Database -> UsuárioREST : resultado: <false>
UsuárioREST -> Frontend : RESPONSE 400 \n{ "error": <<BadRequest>> }
Frontend -> Usuário : Ponto de Coleta não encontrado
@enduml


# UC4:​ Denunciar ponto de coleta 
@startuml
title **UC4 - DENUNCIAR PONTO DE COLETA - FLUXO NORMAL**
autonumber
actor Usuário
boundary Frontend
participant Usuário as UsuárioREST <<rest module>>
database Database
Usuário -> Frontend : denuncia ponto de coleta
Frontend -> UsuárioREST : POST /denuncia {\n    "id": <id>,\n    "usuario": <usuario>,\n    "descricao": <descricao>\n}
UsuárioREST -> Database : insere <id>, <usuario>, <descricao>,\n<data> na tabela de denuncia\n
UsuárioREST -> Database : insere <id> (ponto), <id> (denuncia)\nna tabela denunciaPonto
Database -> UsuárioREST : resultado: <<object>><denuncia>
UsuárioREST -> Frontend : RESPONSE 200\n<<object>><denuncia>
Frontend -> Usuário: exibe mensagem informando que a\ndenuncia foi criada com sucesso
@enduml


@startuml
title **UC4 - DENUNCIAR PONTO DE COLETA - PONTO DE COLETA NÃO ENCONTRADO**
autonumber
actor Usuário
boundary Frontend
participant Usuário as UsuárioREST <<rest module>>
database Database
Usuário -> Frontend : denuncia ponto de coleta
Frontend -> UsuárioREST : POST /denuncia {\n    "id": <id>,\n    "usuario": <usuario>,\n    "descricao": <descricao>\n}
UsuárioREST -> Database : insere <id>, <usuario>, <descricao>,\n<data> na tabela de denuncia\n
UsuárioREST -> Database : insere <id> (ponto), <id> (denuncia)\nna tabela denunciaPonto
Database -> UsuárioREST : resultado: <false>
UsuárioREST -> Frontend : resultado: RESPONSE 404\nPonto não encontrado
Frontend -> Usuário : não existem pontos de coleta disponíveis \npara o material selecionado
@enduml


@startuml
title **UC4 - DENUNCIAR PONTO DE COLETA - DESCRIÇÃO VAZIA**
autonumber
actor Usuário
boundary Frontend
participant Usuário as UsuárioREST <<rest module>>
database Database
Usuário -> Frontend : denuncia ponto de coleta
Frontend -> UsuárioREST : POST /denuncia {\n    "id": <id>,\n    "usuario": <usuario>,\n    "descricao": <descricao>\n}
UsuárioREST -> Database : insere <id>, <usuario>, <descricao>,\n<data> na tabela de denuncia\n
UsuárioREST -> Database : insere <id> (ponto), <id> (denuncia)\nna tabela denunciaPonto
Database -> UsuárioREST : resultado: <false>
UsuárioREST -> Frontend : RESPONSE 400\n{ "error": <BadRequest> }
Frontend -> Usuário : o campo “descrição” está vazio, por favor,\npreencha o campo para prosseguir com a denúncia
@enduml


# UC5:​ Julgar denúncia 
@startuml
title **UC5 - JULGAR DENUNCIA - FLUXO NORMAL**
autonumber
actor Administrador
boundary Frontend
participant Administrador as AdministradorREST <<rest module>>
database Database
Administrador -> Frontend : Administrador aceita denuncia
Frontend -> AdministradorREST : POST /julgamento {\n    "id": <id>,\n    "usuario": <usuario>,\n    "resultado": <true>\n}
AdministradorREST -> Database : exclui <ponto> com <id> do banco
AdministradorREST -> Database : atualiza <denuncia> no banco
Database -> AdministradorREST  : resultado: <true>
AdministradorREST -> Frontend : RESPONSE 200
Frontend -> Administrador : denuncia aceita com sucesso
@enduml


@startuml
title **UC5 - JULGAR DENUNCIA - PONTO DE COLETA NÃO ENCONTRADO**
autonumber
actor Administrador
boundary Frontend
participant Administrador as AdministradorREST <<rest module>>
database Database
Administrador -> Frontend : Administrador aceita denuncia
Frontend -> AdministradorREST : POST /julgamento {\n    "id": <id>,\n    "usuario": <usuario>,\n    "resultado": <true>\n}
AdministradorREST -> Database : exclui <ponto> com <id> do banco
Database -> AdministradorREST  : resultado: <false>
AdministradorREST -> Frontend : RESPONSE 404 \nerror { error: "Ponto não encontrado" }
Frontend -> Administrador : Ponto de coleta informado não existe
@enduml


@startuml
title **UC5 - JULGAR DENUNCIA - DENUNCIA REJEITADA**
autonumber
actor Administrador
boundary Frontend
participant Administrador as AdministradorREST <<rest module>>
database Database
Administrador -> Frontend : Administrador rejeita denuncia
Frontend -> AdministradorREST : POST /julgamento {\n    "id": <id>,\n    "usuario": <usuario>,\n    "resultado": <false>\n}
AdministradorREST -> Database : mantêm <ponto> com <id> do banco
AdministradorREST -> Database : atualiza <denuncia> no banco
Database -> AdministradorREST  : resultado: <false>
AdministradorREST -> Frontend : RESPONSE 200
Frontend -> Administrador : denuncia rejeitada pelo administrador
@enduml





