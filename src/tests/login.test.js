const { TestScheduler } = require('jest')
const requestPostBody = require('./config/request')

test('Create a new user', async () => {
    const user = {
        user: "userTeste",
        password: "teste123"
    }
    const res = await requestPostBody('user', user, {})
    expect(res.user).toEqual("userTeste")
    expect(res.role).toEqual("Usuário")
    expect(res.token).not.toBeUndefined()
})

test('Create the same user', async() => {
    const user = {
        user: "userTeste",
        password: "teste123"
    }
    expect((await requestPostBody('user', user, {})).toString()).toEqual('StatusCodeError: 400 - {"message":"User already exists"}')
})