const request = require('request-promise')
const http = require('http')
const querystring = require('querystring');
const { urlencoded } = require('express');

async function getToken(user) {
  var token = {}
  switch (user) {
    case "Usuário":
      token = await request.post('http://localhost:3333/login', { json: true, body: { user: "testUser", password: "1231fesad1h28d723" } })
      break

    case "Empresa":
      token = await request.post('http://localhost:3333/login', { json: true, body: { user: "testEnterprise", password: "1231fe412d12dh28d723" } })
      break

    case "Administrador":
      token = await request.post('http://localhost:3333/login', { json: true, body: { user: "testAdministrator", password: "1231fe41234das1h28d723" } })
      break
  
    default:
      token = await request.post('http://localhost:3333/login', { json: true, body: { user: "testUser", password: "1231fesad1h28d723" } })
      break
  }

  return token
}

async function requestGetBodyWithToken(url, body, headers, user) {
  headers.token = await getToken(user)
  return await request.get(`http://localhost:3333/${url}`, { json: true, body, headers })
}

async function requestGetBody(url, body, headers) {
  return await request.get(`http://localhost:3333/${url}`, { json: true, body, headers })
}

async function requestPostBody(url, body, headers) {
  return await request.post(`http://localhost:3333/${url}`, { json: true, body, headers }).catch((err) => { return err })
}

module.exports = requestGetBodyWithToken
module.exports = requestGetBody
module.exports = requestPostBody