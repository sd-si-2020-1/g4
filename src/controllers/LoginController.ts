import { Request, Response } from 'express'
import Auth from '../services/auth/auth'
import knex from '../database/connection'

class LoginController {
    async createUser(request: Request, response: Response) {
        const { user, password } = request.body
        const exist = await knex('users').where('user', user).first()
        if(exist) {
            return response.status(400).json({ message: 'User already exists' })
        }
        const login = {
            user,
            password,
            role: 'Usuário'
        }
        const trx = await knex.transaction()
        const insert = await trx('users').insert(login)
      
        await trx.commit()
        
        const tokenAcess = new Auth()
        const token = tokenAcess.getToken({
            user,
            role: 'Usuário',
            id: insert[0]
        })

        return response.status(201).json({ 
            id: insert[0],
            user,
            role: 'Usuário',
            token
        })
    }
    
    async createEnterprise(request: Request, response: Response) {
        const { user, password ,name, email, whatsapp, latitude, longitude, city, uf, items } = request.body
        const exist = await knex('users').where('user', user).first()
        if(exist) {
            return response.status(400).json({ message: 'User already exists' })
        }
        const login = {
            user,
            password,
            role: 'Empresa'
        }
        const point = {
            name,
            email,
            whatsapp,
            latitude,
            longitude, 
            city,
            uf,
            image: request.file.filename
        }
        const trx = await knex.transaction()
        const insertId = await trx('points').insert(point)
        const insertLogin = await trx('users').insert(login)
        const pointItems = items.split(',').map((itemId: Number) => {
            return {
                itemId,
                pointId: insertId[0]
            }
        })
        await trx('pointItems').insert(pointItems)

        await trx.commit()

        const tokenAcess = new Auth()
        const token = tokenAcess.getToken({
            user,
            role: 'Enterprise',
            id: insertLogin[0]
        })

        return response.status(201).json({ 
            userId: insertLogin[0],
            user: insertLogin[1],
            role: insertLogin[3],
            pointId: insertId[0], 
            ...point,
            token
        })
    }

    async login(request: Request, response: Response) {
        const { user, password } = request.body

        const userBD = await knex('users').where('user', user).first()

        if (!(userBD.password == password)) {
            return response.status(400).json({ message: "Usuário ou senha inválido" })
        }

        const token = new Auth().getToken(userBD)

        return response.status(200).json({ token, user })
    }
}

export default LoginController
