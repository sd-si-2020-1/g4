import { Request, Response } from 'express'
import knex from '../database/connection'
import Auth from '../services/auth/auth'

class DenunciaController {
    async index(request: Request, response: Response) {
        const userId = new Auth().decode(String(request.header('token'))).userId
        const denuncias = await knex('denuncia').where('userId', userId)

        return response.status(200).json(denuncias)
    }

    async create(request: Request, response: Response) {
        const { description, pointId } = request.body
        const userId = new Auth().decode(String(request.header('token'))).userId

        const exist = await knex('points').where('id', pointId).first()

        if(!exist) {
            return response.status(404).json({
                message: 'Point doesn\'t exist',
                errCode: 404
            })
        }

        const insertDenuncia = await knex('denuncia').insert({
            userId: userId,
            pointId: pointId,
            description: description
        })

        return response.status(201).json({
            denunciaId: insertDenuncia[0],
            pointId,
            description
        })
    }

    async judge(request: Request, response: Response) {
        const { denunciaId, judge } = request.body

        if (!judge) {
            await knex('denuncia').where('id', denunciaId).update({ judge: "Indeferida" })
            return response.status(200).json({ denunciaId, judge: "Indeferida" })
        }
        const userDenuncia: any = await knex('denuncia').where('id', denunciaId).select("*")
        if (userDenuncia.length == 0) {
            return response.status(400).json({ message: "Denuncia não encontrada" })
        }

        var point = userDenuncia.map((row: any) => { return row.pointId })[0]

        const alreadyJudge = await knex('denuncia').where('pointId', point).andWhere('judge', 'Deferida').select("*")
        if (alreadyJudge.length > 0) {
            return response.status(200).json({ message: "Point already judge." })
        }
        
        const blackList = await knex('points').where('id', point).select("*")
        const trx = await knex.transaction()
        await trx('denuncia').where('pointId', point).andWhereNot('id', denunciaId).andWhere('judge', null).update({ judge: "Finalizada" })
        await trx('denuncia').where('id', denunciaId).update({ judge: "Deferida" })
        await trx('points').where('id', point).delete()
        trx.commit()
        
        console.log(blackList)
        await knex('blackList').insert({
            name: blackList[0].name,
            email: blackList[0].email,
            whatsapp: blackList[0].whatsapp
        })

        // julga denuncia, se positiva, remove ponto e user adicionando na blacklist
        return response.status(200).json({})
    }
}

export default DenunciaController
