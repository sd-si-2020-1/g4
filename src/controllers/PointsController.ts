import { Request, response, Response } from 'express'
import knex from '../database/connection'

class PointsController {
    async create(request: Request, response: Response) {
        const { name, email, whatsapp, latitude, longitude, city, uf, items } = request.body
        const trx = await knex.transaction()
        const point = {
            name,
            email,
            whatsapp,
            latitude,
            longitude, 
            city,
            uf,
            image: request.file.filename
        }

        const insertId = await trx('points').insert(point)

        const pointIems = items.split(',').map((itemId: Number) => {
            return {
                itemId,
                pointId: insertId[0]
            }
        })

        await trx('pointItems').insert(pointIems)

        await trx.commit()

        return response.status(201).json({ id: insertId[0], ...point })
    }

    async show(request: Request, response: Response) {
        const { id } = request.params

        const point = await knex('points').where('id', id).first()

        if (!point) {
            return response.status(400).json({ message: 'Point not found.' })
        }

        const serializedPoint = {
            ...point,
            imageUrl: `http://localhost:3333/uploads/${point.image}`
        }

        const items = await knex('items').join('pointItems', 'items.id', '=', 'pointItems.itemId').where('pointItems.pointId', id)

        return response.json({ serializedPoint, items })
    }

    async index(request: Request, response: Response) {
        const { city, uf, items } = request.query
        const parsedItems = String(items).split(',').map(item => Number(item.trim()))
        const points = await knex('points')
            .join('pointItems', 'points.id', '=', 'pointItems.pointId')
            .whereIn('pointItems.itemId', parsedItems)
            .where('city', String(city))
            .where('uf', String(uf))
            .distinct()
            .select('points.*')

        const serializedPoints = points.map(point => {
            return {
                ...point,
                imageUrl: `http://localhost:3333/uploads/${point.image}`
            }
        })

        return response.json(serializedPoints)
    }
}

export default PointsController