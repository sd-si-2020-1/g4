import express from 'express'

import multer from 'multer'
import multerConfig from './config/multer'

import PointsController from './controllers/PointsController'
import ItemsController from './controllers/ItemsController'
import LoginController from './controllers/LoginController'
import JWTAuth from './services/auth/auth'
import DenunciaController from './controllers/DenunciaController'
import Validations from './config/Validations'

const routes = express.Router()
const upload = multer(multerConfig)
const pointsController = new PointsController()
const itemsController = new ItemsController()
const loginController = new LoginController()
const denunciaController = new DenunciaController()
const jwtAuth = new JWTAuth() 
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('../docs/Swagger/swagger.json');

/* ------------ ROTAS ------------ */
/* ------ items ------ */

routes.get('/items', jwtAuth.validToken, itemsController.index)


/* ------ points ------ */

routes.post('/points', upload.single('image'), Validations.pointsPostValidation, jwtAuth.validTokenBusiness, pointsController.create)

routes.get('/points', Validations.pointsGetSearch, jwtAuth.validToken, pointsController.index)

routes.get('/points/:id', Validations.pointsGetId, jwtAuth.validToken, pointsController.show)


/* ------ login ------ */

routes.post('/user', Validations.loginUser, loginController.createUser)

routes.post('/enterprise', upload.single('image'), Validations.loginEnterprise, loginController.createEnterprise)

routes.post('/login', Validations.login, loginController.login)

/* ------ denuncia ------ */

routes.post('/denuncia', Validations.denunciaPostDenuncia, denunciaController.create)

routes.get('/denuncia', Validations.denunciaGetDenuncia, denunciaController.index)

routes.post('/judge', Validations.denunciaJudge, denunciaController.judge)


/* ------ Documentação Swagger ------ */

routes.use('/api-docs', swaggerUi.serve)

routes.get('/api-docs', swaggerUi.setup(swaggerDocument))

export default routes