require('dotenv').config()
import cors from 'cors'
import express from 'express'
import routes from './routes'
import path from 'path'
import { errors } from 'celebrate'

const app = express()

app.use(cors())
app.use(express.json())
app.use(routes)

app.use('/assets', express.static(path.resolve(__dirname, 'assets')))
app.use('/uploads', express.static(path.resolve(__dirname, 'assets', 'uploads')))

app.use(errors())

app.listen(3333)
