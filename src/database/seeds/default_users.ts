import Knex from 'knex'

export async function seed(knex:Knex) {
    return await knex('users').insert([
        { user: "testUser", password: "1231fesad1h28d723", role: "Usuário" },
        { user: "testEnterprise", password: "1231fe412d12dh28d723", role: "Empresa" },
        { user: "testAdministrator", password: "1231fe41234das1h28d723", role: "Administrador" }
    ])
}