import Knex from 'knex'

export async function up(knex: Knex) {
    return knex.schema.createTable('denuncia', table => {
        table.increments('id').primary()
        table.string('userId').notNullable()
        table.string('pointId').notNullable()
        table.string('description').notNullable()
        table.string('judge').defaultTo(null)
    })
}

export async function down(knex: Knex) {
    return knex.schema.dropTable('denuncia')
}