import Knex from 'knex'

export async function up(knex: Knex) {
    return knex.schema.createTable('userPoints', table => {
        table.increments('id').primary()
        table.integer('pointId').notNullable().references('id').inTable('points')
        table.string('loginId').notNullable().references('id').inTable('users')
    })
}

export async function down(knex: Knex) {
    return knex.schema.dropTable('userPoints')
}