import Knex from 'knex'

export async function up(knex: Knex) {
    return knex.schema.createTable('blackList', table => {
        table.increments('id').primary()
        table.string('name').nullable()
        table.string('email').nullable()
        table.string('whatsapp').nullable()
    })
}

export async function down(knex: Knex) {
    return knex.schema.dropTable('blackList')
}