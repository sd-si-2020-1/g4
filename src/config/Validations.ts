import { celebrate, Joi } from "celebrate";

class Validation {

    /* --------- Points --------- */
    //#region points
    static pointsPostValidation = celebrate({ 
        body: Joi.object().keys({
            name: Joi.string().required(),
            email: Joi.string().required(),
            whatsapp: Joi.string().required(),
            latitude: Joi.string().required(),
            longitude: Joi.string().required(),
            city: Joi.string().required(),
            uf: Joi.string().required().max(2),
            items: Joi.string().required()
        }),
        headers: Joi.object().keys({
            token: Joi.string().required()
        }).unknown()
    },
    {
        abortEarly: false
    })

    static pointsGetSearch = celebrate({ 
        query: Joi.object().keys({
            city: Joi.string().required(),
            uf: Joi.string().required().max(2),
            items: Joi.string().required()
        }),
        headers: Joi.object().keys({
            token: Joi.string().required()
        }).unknown()
    },
    {
        abortEarly: false
    })

    static pointsGetId = celebrate({ 
        query: Joi.object().keys({
            city: Joi.string().required(),
            uf: Joi.string().required().max(2),
            items: Joi.string().required()
        }),
        headers: Joi.object().keys({
            token: Joi.string().required()
        }).unknown()
    },
    {
        abortEarly: false
    })
    //#endregion points

    /* --------- Login --------- */
    //#region login
    static loginUser = celebrate({ 
        body: Joi.object().keys({
            user: Joi.string().required(),
            password: Joi.string().required()
        })
    },
    {
        abortEarly: false
    })

    static loginEnterprise = celebrate({ 
        body: Joi.object().keys({
            name: Joi.string().required(),
            email: Joi.string().required(),
            whatsapp: Joi.string().required(),
            latitude: Joi.string().required(),
            longitude: Joi.string().required(),
            city: Joi.string().required(),
            uf: Joi.string().required().max(2),
            items: Joi.string().required(),
            user: Joi.string().required(),
            password: Joi.string().required()
        })
    },
    {
        abortEarly: false
    })

    static login = celebrate({ 
        body: Joi.object().keys({
            user: Joi.string().required(),
            password: Joi.string().required()
        })
    },
    {
        abortEarly: false
    })
    //#endregion login

    /* --------- Denuncia --------- */
    //#region denuncia
    static denunciaPostDenuncia = celebrate({ 
        body: Joi.object().keys({
            pointId: Joi.string().required(),
            description: Joi.string().required()
        }),
        headers: Joi.object().keys({
            token: Joi.string().required()
        }).unknown()
    },
    {
        abortEarly: false
    })

    static denunciaGetDenuncia = celebrate({
        headers: Joi.object().keys({
            token: Joi.string().required()
        }).unknown()
    },
    {
        abortEarly: false
    })

    static denunciaJudge = celebrate({ 
        body: Joi.object().keys({
            denunciaId: Joi.number().required(),
            judge: Joi.boolean().required()
        }),
        headers: Joi.object().keys({
            token: Joi.string().required()
        }).unknown()
    },
    {
        abortEarly: false
    })
    //#endregion denuncia
}

export default Validation