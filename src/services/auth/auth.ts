import jwt from 'jsonwebtoken'

class Auth {
    static roles = ['Usuário', 'Empresa', 'Administrador']

    validToken(request: any, response: any, nextHandler: any) {
        const token = request.header('token')

        try {
            let payload: any = jwt.verify(token, String(process.env.ACCESS_TOKEN_SECRET))
            if (!Auth.roles.includes(payload.role)) {
                throw new Error('Role doesn\'t have authorization for this action')
            }
            if (Date.now() > new Date(payload.exp).getTime()) {
                console.log(payload)
                throw new Error(`Not authorized exp: ${payload.exp}`)
            }
            nextHandler()
        } catch (err) {
            response.status(401).send(err.message)
        }
    }

    validTokenBusiness(request: any, response: any, nextHandler: any) {
        const token = request.header('token')

        try {
            let payload: any = jwt.verify(token, String(process.env.ACCESS_TOKEN_SECRET))
            if (payload.role !== 'Empresa') {
                throw new Error('Role doesn\'t have authorization for this action')
            }
            if (Date.now() > new Date(payload.exp).getTime()) {
                throw new Error('Not authorized')
            }
            nextHandler()
        } catch (err) {
            response.status(401).send(err.message)
        }
    }

    validTokenAdministrator(request: any, response: any, nextHandler: any) {
        const token = request.header('token')

        try {
            let payload: any = jwt.verify(token, String(process.env.ACCESS_TOKEN_SECRET))
            if (payload.role !== 'Empresa') {
                throw new Error('Role doesn\'t have authorization for this action')
            }
            if (Date.now() > new Date(payload.exp).getTime()) {
                throw new Error('Not authorized')
            }
            nextHandler()
        } catch (err) {
            response.status(401).send(err.message)
        }
    }

    getToken(user: any) : String {
        return jwt.sign({ userId: user.id, login: user.login, role: user.role, iat: Date.now(), exp: Date.now() + (2*60*60*1000) }, String(process.env.ACCESS_TOKEN_SECRET))
    }

    decode(token: string): any {
        return jwt.verify(token, String(process.env.ACCESS_TOKEN_SECRET))
    }    
}

export default Auth