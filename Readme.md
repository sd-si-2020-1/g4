**PColeta**

O presente sistema tem como intuito o cadastro de pontos de coleta de materiais como: óleo, pilhas e celulares. Assim quando um usuário acessar o sistema consegue localizar o ponto de coleta mais próximo a ele que faça a coleta do material.

**Install**

```sh
$ git clone https://gitlab.com/sd-si-2020-1/g4.git
$ yarn install
$ yarn migrate
$ yarn seed
```

Commando em uma linha com PowerShell
```sh
$ git clone https://gitlab.com/sd-si-2020-1/g4.git; yarn install; yarn migrate; yarn seed
```

Comando em uma linha com Cmd
```sh
$ git clone https://gitlab.com/sd-si-2020-1/g4.git && yarn install && yarn migrate && yarn seed
```

**Run**
```sh
$ cd (diretório da clonagem)
$ yarn dev
```

OBS: O programa será iniciado na porta 3333, caso ela já esteja sendo utilizada, alterar a porta no arquivo server.ts.
