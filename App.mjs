import jwt from 'jsonwebtoken'

const secret = 'swsh23hjddnns'

const users = [
    {
        login: "gabriel",
        password: "123123"
    }
]

var genToken = jwt.sign({ userId: 1, login: users[0].login, role: 'Empresa', iat: Date.now(), exp: Date.now() + (2*60*60*1000) }, secret)

console.log(genToken)

var tokenDecoded = jwt.verify(genToken, secret)

console.log(tokenDecoded)
